package org.htwdd.main;

import java.util.Random;

public class Bevoelkerung {
	

	int potential_customers;
	double[] customers;
	Random r;
	
	public Bevoelkerung(int potential_customers) {
		this.potential_customers = potential_customers;
		customers = new double[potential_customers];
		this.r = new Random();
	}
	

	
	public int getBevoelkerung(double temperature) {
		if(temperature < 10) {
			return (int) (potential_customers * 0.3 * (0.8+0.4*r.nextDouble()));
		}else if(temperature > 26) {
			return (int) (potential_customers * 1.1);
		}else {
			return potential_customers;
		}
	}
	
	
}
