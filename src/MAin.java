import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.htwdd.main.Bevoelkerung;

import umontreal.iro.lecuyer.probdist.PiecewiseLinearEmpiricalDist;

public class MAin {

	public static void main(String[] args) {
		
		ConcurrentMap<Integer, Double> map = new ConcurrentHashMap();
		/*int count = 0;
		Portionsgroesse portion = new Portionsgroesse();
		int month= 5;
		Map<Integer,Integer> map = new HashMap();
		while (count < 10000) {
			int portio = portion.getPortionsgroesse(month, Math.random());
			if(!map.containsKey(portio))map.put(portio, 1);
			else map.put(portio, map.get(portio) + 1);			
			count++;			
		}*/

		Random r = new Random(234);
		int potential_customers = 5000;
		int kundenzahl;
		
		double ice_price = 1.0;
		double ice_cost = 0.8;
		double standmiete = 3000;
		double ordered_amount = 4000;
		
	
		JahresTemperaturen temp = new JahresTemperaturen();	
		temp.badYear();
		Portionsgroesse p = new Portionsgroesse(temp);
		for(int runs = 0; runs < 1000; runs++) {
			Bevoelkerung b = new Bevoelkerung(potential_customers);
			//1. �bliche Temperatur
			for(int month = 1;month<=12;month++) {
				int portion_sum = 0;
				double temperature = temp.getTemperatur(month);			
				//2. zuf�llige Kundenzahl				
				kundenzahl = b.getBevoelkerung(temperature);
				
				//3. je Kunde Portionsgroesse				
				for(int i = 0; i< kundenzahl;i++) {
					portion_sum += p.getPortionsgroesse(month, r.nextDouble());
				}

				double gewinn = portion_sum * ice_price - ice_cost * ordered_amount;				
				if(map.get(month) != null) {
					map.put(month, map.get(month) + gewinn);
				}else {
					map.put(month,gewinn);
				}
			}
		}
				
		map.forEach((k,v) -> {
			String s = v/1000 + "";
			System.out.println(s.replace(".", ","));
		});

	}

}
