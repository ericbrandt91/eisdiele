import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class JahresTemperaturen {
	
	
	Map<Integer, Double> months;

	JahresTemperaturen(){
		months = new HashMap();		
		
	}
	
	
	
	
	public void badYear() {
		months.put(1, -2.1);
		months.put(2,2.5 );
		months.put(3, 0.9);
		months.put(4, 3.5);
		months.put(5, 8.5);
		months.put(6, 18.7);
		months.put(7,17.5);
		months.put(8, 13.3);
		months.put(9, 13.4);
		months.put(10, 6.7);
		months.put(11, 2.2);
		months.put(12, -0.9);
	}
	
	public void goodYear() {
		months.put(1, 2.1);
		months.put(2,3.5 );
		months.put(3, 7.4);
		months.put(4, 5.0);
		months.put(5, 16.8);
		months.put(6, 25.0);
		months.put(7,24.0);
		months.put(8, 21.7);
		months.put(9, 18.3);
		months.put(10, 13.2);
		months.put(11, 7.0);
		months.put(12, 3.4);
	}
	
	
	
	public double getTemperatur(int month) {
		return this.months.get(month);
	}
}
