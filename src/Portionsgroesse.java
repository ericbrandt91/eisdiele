import java.util.HashMap;
import java.util.Map;

public class Portionsgroesse {
	
	JahresTemperaturen temp;
	
	public Portionsgroesse(JahresTemperaturen temp) {
		this.temp = temp;	
	}
	
	
	public int getPortionsgroesse(int month, double portionsprob) {
		
		double temperature = temp.months.get(month);
		if(temperature < 15) {
			return getPortionCase1(portionsprob);
		} else if(temperature <25 && temperature > 15 ) {
			return getPortionCase2(portionsprob);
		}else {
			return getPortionCase3(portionsprob);
		}
			
		
	}
	
	private int getPortionCase1(double portionsProb) {
		if(portionsProb <0.15 & portionsProb > 0.03)return 2;		
		if(portionsProb <= 0.03) return 3;
		return 1;
	}
	
	private int getPortionCase2(double portionsProb) {
		if(portionsProb <0.25 & portionsProb > 0.125)return 2;		
		if(portionsProb <= 0.125) return 3;
		return 1;
	}
	
	private int getPortionCase3(double portionsProb) {
		if(portionsProb <0.5 & portionsProb > 0.2)return 2;		
		if(portionsProb <= 0.2) return 3;
		return 1;
	}
	
}
